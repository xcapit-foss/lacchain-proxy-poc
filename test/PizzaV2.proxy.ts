import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Pizza, PizzaV2 } from '../typechain-types';

describe('PizzaV2 Proxy', function () {
  const aNumberOfSlices = 8;

  async function deployPizza() {
    const [owner, wallet1, wallet2] = await ethers.getSigners();
    const Pizza = await ethers.getContractFactory('Pizza');
    const PizzaV2 = await ethers.getContractFactory('PizzaV2');
    const pizza = await upgrades.deployProxy(Pizza, [aNumberOfSlices], { initializer: 'initialize' });
    const pizzaV2 = await upgrades.upgradeProxy(pizza.address, PizzaV2);

    return { pizzaV2, owner, wallet1, wallet2 };
  }

  it('Deployed whit default values', async function () {
    const { pizzaV2, owner } = await loadFixture(deployPizza);
    expect(await pizzaV2.owner()).to.equal(owner.address);
    expect(await pizzaV2.slices()).to.equal(aNumberOfSlices);
  });

  it('eat slice', async function () {
    const { pizzaV2 } = await loadFixture(deployPizza);

    await pizzaV2.eatSlice();

    expect(await pizzaV2.slices()).to.equal(aNumberOfSlices - 1);
  });

  it('only owner can upgrade', async function () {
    const { pizzaV2, wallet1, wallet2 } = await loadFixture(deployPizza);

    await expect((pizzaV2 as Pizza).connect(wallet1).upgradeTo(wallet2.address)).to.revertedWith(
      'Ownable: caller is not the owner'
    );
  });

  it('contract already initialize', async function () {
    const { pizzaV2 } = await loadFixture(deployPizza);

    await expect(pizzaV2.initialize(aNumberOfSlices)).to.revertedWith('Initializable: contract is already initialized');
  });

  it('refill slice', async function () {
    const { pizzaV2 } = await loadFixture(deployPizza);
    await pizzaV2.eatSlice();

    await pizzaV2.refillSlice();

    expect(await pizzaV2.slices()).to.equal(aNumberOfSlices);
  });

  it('set dummy', async function () {
    const aDummyValue = 7;
    const { pizzaV2 } = await loadFixture(deployPizza);

    await pizzaV2.setDummy(aDummyValue);

    expect(await pizzaV2.dummy()).to.equal(aDummyValue);
  });

});
