import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import { ethers } from 'hardhat';
import { PizzaV2 } from '../typechain-types';

describe('PizzaV2 Impl', function () {
  const aNumberOfSlices = 8;

  async function deployPizza() {
    const [owner, anotherAccount] = await ethers.getSigners();
    const PizzaV2 = await ethers.getContractFactory('PizzaV2');
    const pizzaV2: PizzaV2 = await PizzaV2.deploy();

    return { pizzaV2, owner, anotherAccount };
  }

  it('Deployed whit default values', async function () {
    const { pizzaV2 } = await loadFixture(deployPizza);

    expect(await pizzaV2.slices()).to.equal(0);
    expect(await pizzaV2.owner()).to.equal(ethers.constants.AddressZero);
  });

  it('Initialize', async function () {
    const { pizzaV2, owner } = await loadFixture(deployPizza);

    await pizzaV2.initialize(aNumberOfSlices);

    expect(await pizzaV2.owner()).to.equal(owner.address);
    expect(await pizzaV2.slices()).to.equal(aNumberOfSlices);
  });

  it('Initialize twice', async function () {
    const { pizzaV2 } = await loadFixture(deployPizza);

    await pizzaV2.initialize(aNumberOfSlices);

    await expect(pizzaV2.initialize(aNumberOfSlices)).to.revertedWith('Initializable: contract is already initialized');
  });

  it('eat slice', async function () {
    const { pizzaV2 } = await loadFixture(deployPizza);
    await pizzaV2.initialize(aNumberOfSlices);

    await pizzaV2.eatSlice();

    expect(await pizzaV2.slices()).to.equal(aNumberOfSlices - 1);
  });

  it('refill slice', async function () {
    const { pizzaV2 } = await loadFixture(deployPizza);
    await pizzaV2.initialize(aNumberOfSlices);
    await pizzaV2.eatSlice();

    await pizzaV2.refillSlice();

    expect(await pizzaV2.slices()).to.equal(aNumberOfSlices);
  });

  it('set dummy', async function () {
    const aDummyValue = 7;
    const { pizzaV2 } = await loadFixture(deployPizza);
    await pizzaV2.initialize(aNumberOfSlices);

    await pizzaV2.setDummy(aDummyValue);

    expect(await pizzaV2.dummy()).to.equal(aDummyValue);
  });
});
