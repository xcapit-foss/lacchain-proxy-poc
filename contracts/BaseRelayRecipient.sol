// SPDX-License-Identifier:MIT
pragma solidity ^0.8.9;

/**
 * A base contract to be inherited by any contract that want to receive relayed transactions
 * A subclass must use "_msgSender()" instead of "msg.sender"
 */
abstract contract BaseRelayRecipient {
    address constant trustedForwarder = 0xa4B5eE2906090ce2cDbf5dfff944db26f397037D;
  /*
   * Forwarder singleton we accept calls from
   */

  /**
   * return the sender of this call.
   * if the call came through our Relay Hub, return the original sender.
   * should be used in the contract anywhere instead of msg.sender
   */
  function _msgSender() internal view virtual returns (address sender) {
    bytes memory bytesRelayHub;
    (, bytesRelayHub) = trustedForwarder.staticcall(abi.encodeWithSignature('getRelayHub()'));

    if (msg.sender == abi.decode(bytesRelayHub, (address))) {
      //sender is RelayHub then return origin sender
      bytes memory bytesSender;
      (, bytesSender) = trustedForwarder.staticcall(abi.encodeWithSignature('getMsgSender()'));

      return abi.decode(bytesSender, (address));
    } else {
      //sender is not RelayHub, so it is another smart contract
      return msg.sender;
    }
  }
}
