// contracts/Dscrow.sol
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import '@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol';
import '@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol';
import './BaseRelayRecipient.sol';

contract Dscrow is Initializable, OwnableUpgradeable, UUPSUpgradeable, BaseRelayRecipient {

  /// @custom:oz-upgrades-unsafe-allow constructor
  constructor() {
    _disableInitializers();
  }

  function initialize(address owner) public initializer {
    __Ownable_init(owner);
    __UUPSUpgradeable_init();
  }

  function _authorizeUpgrade(address) internal override onlyOwner {}

  function _msgSender() internal view override(ContextUpgradeable, BaseRelayRecipient) returns (address) {
    return BaseRelayRecipient._msgSender();
  }
}
