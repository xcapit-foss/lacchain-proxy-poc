import { env } from 'process';
import { HardhatUserConfig } from 'hardhat/config';
import '@nomicfoundation/hardhat-toolbox';
import '@openzeppelin/hardhat-upgrades';
import dotenv from 'dotenv';

dotenv.config();

const { RPC_URL, PRIVATE_KEY, ETHERSCAN_API_KEY } = env;

const config: HardhatUserConfig = {
  solidity: { compilers: [{ version: '0.8.9' }, { version: '0.8.18' }, { version: '0.8.20' }] },
  networks: {
    hardhat: {},
    sepolia: {
      url: RPC_URL,
      accounts: [`0x${PRIVATE_KEY}`],
    },
  },
  etherscan: {
    apiKey: ETHERSCAN_API_KEY,
  },
};

export default config;
