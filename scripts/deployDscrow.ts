import { ethers, upgrades } from 'hardhat';
import { GasModelProvider, GasModelSigner } from '@lacchain/gas-model-provider';

async function main() {
  const expiration = 1836394529;
  const nodeAddress = '';
  const privateKey = '';
  const ownerAddress = ''; // Debemos usar la address de la privada definida arriba

  const provider = new GasModelProvider('http://10.20.71.94');
  const signer = new GasModelSigner(privateKey, provider, nodeAddress, expiration);
  const Dscrow = await ethers.getContractFactory('Dscrow', signer);

  console.log('Deploying Dscrow...');
  const dscrow = await upgrades.deployProxy(Dscrow, [ownerAddress], { initializer: 'initialize' });
  await dscrow.deployed();
  console.log('Dscrow deployed to:', dscrow.contractAddress);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
